package Gui;

/**
 * Created by Mlody on 2016-06-10.
 */
public class BossDamageHandler implements DamageHandler{
    private DamageHandler nextDamageHandler;
    public void setNextDamageHandler(DamageHandler damageHandler) {
        nextDamageHandler=damageHandler;
    }

    public void handleDamage(Enemy enemy) {
        Screen.health-=(enemy.enemyHealth+30);
    }
}
