package Gui;

import javax.swing.*;
import java.awt.*;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.io.File;

/**
 * Created by Mlody on 2016-05-07.
 */
public class Screen extends JPanel implements Runnable {

    public Thread gameloop = new Thread(this);

    public static boolean isFirst=true;
    public static boolean isDebug=false;
    public static boolean isWin=false;


    public static Point maus = new Point(0,0);
    public static Store store;
    public static Room room;

    public static Enemy enemies[]= new Enemy[100];

    public static int myWidth, myHeight;
    public static int coinage=10, health=100;
    public static int killed=0, killedToWin=0, level=1;
    public static int winTime=4000, winFrame=0;
    public static String[] enemylist;

    public int maxlevel=3;

    public static Image[] tileset_ground=new Image[100];
    public static Image[] tileset_air= new Image[100];
    public static Image[] tileset_res = new Image[100];
    public static Image[] tileset_enemy = new Image[100];
    public static DamageController damageController=new DamageController();

    public Save save;

    public Screen(){

    }

    public Screen(Frame frame){
        frame.addMouseListener(new KeyHandler());
        frame.addMouseMotionListener(new KeyHandler());
        gameloop.start();
    }

    public void define(){
        room = new Room();
        save = Save.getInstanceOfSave();
        store = new Store();

        for(int i =0; i<tileset_ground.length;i++){
            tileset_ground[i]=new ImageIcon("res/tileset_ground.png").getImage();
            tileset_ground[i]= createImage(new FilteredImageSource(tileset_ground[i].getSource(),new CropImageFilter(0,26*i,26,26)));
        }
        for(int i =0; i<tileset_air.length;i++){
            tileset_air[i]=new ImageIcon("res/tileset_air.png").getImage();
            tileset_air[i]= createImage(new FilteredImageSource(tileset_air[i].getSource(),new CropImageFilter(0,26*i,26,26)));
        }
        for(int i =0; i<tileset_enemy.length;i++){
            tileset_enemy[i]=new ImageIcon("res/tileset_enemy.png").getImage();
            tileset_enemy[i]= createImage(new FilteredImageSource(tileset_enemy[i].getSource(),new CropImageFilter(0,26*i,26,26)));
        }

        tileset_res[0] = new ImageIcon("res/cell.png").getImage();
        tileset_res[1] = new ImageIcon("res/heart.png").getImage();
        tileset_res[2]= new ImageIcon("res/coin.png").getImage();

        save.loadSave(new File("save/mission"+level+".xgame"));

        for (int i = 0; i < enemies.length; i++) {
            enemies[i]= new Enemy();
        }
    }

    public void paintComponent(Graphics graphics){
        SimpleGraphics g= new SimpleGraphics(graphics);
        if(isFirst){
            myWidth=getWidth();
            myHeight=getHeight();
            define();
            isFirst=false;
        }

        addBackground(g);
        room.draw(g);
        for (int i = 0; i < enemies.length; i++) {
            if(enemies[i].inGame){
                enemies[i].draw(g);
            }
        }
        store.draw(g);
        winningCondition(g);
    }





    public void addBackground(SimpleGraphics g){
        g.setColor(new Color(50,50,50));
        g.fillRect(0,0,getWidth(),getHeight());
    }

    public void winningCondition(SimpleGraphics g){
        if(health <1){
            g.setColor(new Color(240,20,20));
            g.fillRect(0,0,myWidth,myHeight);
            g.setColor(new Color(255,255,255));
            g.setFont(new Font("Courier New",Font.BOLD,14));
            g.drawString("GAME OVER",10,10);
        }
        if(isWin){
            g.setColor(new Color(240, 20, 20));
            g.fillRect(0, 0, myWidth, myHeight);
            g.setColor(new Color(255, 255, 255));
            g.setFont(new Font("Courier New", Font.BOLD, 14));
            if(level==maxlevel) {
                g.drawString("You won whole game", 10, 10);
            }else{
                g.drawString("You won, wait for new level", 10, 10);
            }
        }
    }


    public int spawnTime =4000, spawnFrame=0;
    public void addEnemy(){
        if(spawnFrame>=spawnTime){
            for (int i = 0; i < enemylist.length; i++) {
                if(!enemies[i].inGame){
                    enemies[i].spawnEnemy(Integer.parseInt(enemylist[i]));
                    break;
                }
            }
            spawnFrame=0;
        }else{
            spawnFrame++;
        }
    }

    public static void hasWon(){
        if(killed==killedToWin){
            isWin = true;
            killed=0;
        }
    }

    public static void addEnemyList(String [] list){
        enemylist=list;
    }

    public void run() {
        while(true){

           if(!isFirst && health>0 && !isWin){
                room.physics();
               addEnemy();
               for (int i = 0; i < enemies.length; i++) {
                   if(enemies[i].inGame){
                       enemies[i].physics();
                   }
               }
           }else{
               if(isWin){
                   if(winFrame>=winTime){
                        if(level>maxlevel){
                            System.exit(0);
                        }else {
                            level++;
                            winFrame = 0;
                            isWin = false;
                            define();
                        }
                   }else{
                       winFrame+=1;
                   }
               }
           }

           repaint();

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
