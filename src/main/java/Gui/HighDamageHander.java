package Gui;

/**
 * Created by Mlody on 2016-06-10.
 */
public class HighDamageHander implements DamageHandler{
    private DamageHandler nextDamageHandler;
    public void setNextDamageHandler(DamageHandler damageHandler) {
        nextDamageHandler=damageHandler;
    }

    public void handleDamage(Enemy enemy) {
        if(enemy.enemyId==2){
            Screen.health-=30;
            if(Screen.coinage>20){
            Screen.coinage-=20;
            }else{
                Screen.coinage=0;
            }
        }else{
            nextDamageHandler.handleDamage(enemy);
        }
    }
}
