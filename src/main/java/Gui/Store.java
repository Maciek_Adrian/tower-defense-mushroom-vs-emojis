package Gui;

import javax.swing.text.BadLocationException;
import java.awt.*;

/**
 * Created by Mlody on 2016-05-20.
 */
public class Store {
    public static int shopWidth = 5;
    public static int buttonSize=52;
    public static int cellSpace=4;
    public static int iconSize = 20;
    public static int[] buttonId = {Value.AIR_SHROOM1,Value.AIR_SHROOM2,Value.AIR_SHROOM3,Value.AIR_SHROOM4,Value.AIR_TRASH};
    public static int[] buttonPrice= {10,20,70,100,0};
    public static int heldId=-1;
    public static int realId=-1;
    public boolean holdsItem=false;
    public Rectangle[] button = new Rectangle[shopWidth];
    public Rectangle buttonHealth;
    public Rectangle buttonCoins;
    public CommandEditor commandEditor = new CommandEditor();

    public Store(){
        define();
    }

    public void define(){
        for(int i=0; i<button.length;i++){
            button[i]= new Rectangle(((Screen.myWidth/2)-((shopWidth*(buttonSize+cellSpace))/2)+((buttonSize+ cellSpace)*i)),Screen.room.block[Screen.room.worldHeight-1][0].y+Screen.room.blockSize*2,buttonSize,buttonSize);
        }
        buttonHealth=new Rectangle(Screen.room.block[0][0].x-1,button[0].y+button[0].height-iconSize*3,iconSize,iconSize);
        buttonCoins=new Rectangle(Screen.room.block[0][0].x-1,button[0].y+button[0].height-iconSize,iconSize,iconSize);
    }

    public void click(int mousebutton) throws BadLocationException {
        for (int i = 0; i < button.length; i++) {
            if(button[i].contains(Screen.maus)){
                heldId=buttonId[i];
                realId=i;
                if(heldId== Value.AIR_TRASH){
                    holdsItem=false;
                }else{
                    holdsItem=true;
                }
            }
        }
        if(mousebutton==1){
            if(holdsItem){
                if(Screen.coinage>=buttonPrice[realId]){
                    commandEditor.executeCommand(new PurchaseCommand(), new MouseAction(heldId,buttonPrice[realId]));
                }
            }
        }else if(mousebutton==2){
            commandEditor.undoLast();
        }else if(mousebutton==3){
            commandEditor.executeCommand(new SellCommand(),new MouseAction(heldId,buttonPrice[realId]));
        }
    }
    public void draw(SimpleGraphics g){
        for(int i=0; i<button.length;i++){
            g.drawImage(Screen.tileset_res[0],button[i]);
            g.drawImage(Screen.tileset_air[buttonId[i]],button[i]);
            if(buttonPrice[i]>0){
                g.setColor(new Color(255,255,255));
                g.setFont(new Font("Courier New",Font.BOLD,14));
                g.drawString(""+buttonPrice[i]+"$",button[i]);
            }
            if(button[i].contains(Screen.maus)){
                g.setColor(new Color(255,255,255,150));
                g.fillRect(button[i]);
            }
        }
        g.drawImage(Screen.tileset_res[1],buttonHealth);
        g.drawImage(Screen.tileset_res[2],buttonCoins);
        g.setFont(new Font("Courier New",Font.BOLD,14));
        g.setColor(new Color(255,255,255));
        g.drawString(""+Screen.health,buttonHealth);
        g.drawString(""+Screen.coinage,buttonCoins);

        if(holdsItem){
            g.drawImage(Screen.tileset_air[heldId],Screen.maus,button[0]);
        }
    }
}
