package Gui;

import java.awt.*;

/**
 * Created by Mlody on 2016-05-07.
 */
public class Block extends Rectangle {
    public int groundId;
    public int airId;
    public int shotEnemy=-1;
    public int loseTime=100, loseFrame=0;
    public Rectangle towerSquare;
    public int towerSquareSize = 100;

    public boolean shoting= false;

    public Block(int x, int y, int width, int height,int groundId, int airId){
        setBounds(x,y,width,height);
        towerSquare= new Rectangle(x-towerSquareSize/2,y-towerSquareSize/2,width+towerSquareSize,height+towerSquareSize);
        this.airId=airId;
        this.groundId=groundId;
    }
    public void draw(SimpleGraphics g){
        g.drawImage(Screen.tileset_ground[groundId],this);
        if(airId!= Value.AIR_EMPTY){
            g.drawImage(Screen.tileset_air[airId],this);
        }
    }
    public void physics(){
        if (shotEnemy!= -1 && towerSquare.intersects(Screen.enemies[shotEnemy])) {
            shoting = true;
        }else {
            shoting=false;
        }
        if(!shoting) {
            if (airId == Value.AIR_SHROOM1 || airId == Value.AIR_SHROOM2 || airId == Value.AIR_SHROOM3 || airId == Value.AIR_SHROOM4) {
                for (int i = 0; i < Screen.enemies.length; i++) {
                    if (Screen.enemies[i].inGame) {
                        if (towerSquare.intersects(Screen.enemies[i])) {
                            shoting = true;
                            shotEnemy = i;
                        }
                    }
                }
            }
        }
        if(shoting){
            if(loseFrame>=loseTime){
                Screen.enemies[shotEnemy].loseHealth(Value.SHROOM_DAMAGE_POINTS[airId]);
                loseFrame=0;
            }else{
                loseFrame+=1;
            }
            if(Screen.enemies[shotEnemy].isDead()){
                System.out.println(Screen.killed);
                shoting=false;
                shotEnemy=-1;
                Screen.hasWon();
            }
        }
    }

    public void fight(SimpleGraphics g){
        if(Screen.isDebug) {
            if (airId == Value.AIR_SHROOM1) {
                g.drawRect(towerSquare.x, towerSquare.y, towerSquare.width, towerSquare.height);
            }
        }
        if(shoting){
           g.setColor(new Color(255, 255, 255));
           g.drawLine(x+(width/2),y+(height/2),Screen.enemies[shotEnemy].x+ (Screen.enemies[shotEnemy].width/2),Screen.enemies[shotEnemy].y +(Screen.enemies[shotEnemy].height/2));
        }

    }
}
