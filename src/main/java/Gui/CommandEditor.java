package Gui;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.util.Stack;

/**
 * Created by Mlody on 2016-06-10.
 */
public class CommandEditor {
    Stack<Command> cs= new Stack();
    public void undoLast() throws BadLocationException {
        if(!cs.empty()) {
            Command c = cs.pop();
            c.undo();
        }
    }
    public void executeCommand(Command c, MouseAction mouseAction) throws BadLocationException {
        cs.push(c);
        c.execute(mouseAction);
    }
}
