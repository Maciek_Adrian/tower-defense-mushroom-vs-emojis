package Gui;

/**
 * Created by Mlody on 2016-06-10.
 */
public class SellCommand implements  Command {
    MouseAction mouseAction;

    public void execute(MouseAction mouseAction) {

        this.mouseAction = mouseAction;
        for (int y = 0; y < Screen.room.block.length; y++) {
            for (int x = 0; x < Screen.room.block[0].length; x++) {
                if (Screen.room.block[y][x].contains(Screen.maus)) {
                    if (Screen.room.block[y][x].groundId != Value.GROUND_ROAD && Screen.room.block[y][x].airId != Value.AIR_EMPTY) {
                        Screen.room.block[y][x].airId = Value.AIR_EMPTY;
                        Screen.coinage += mouseAction.price;
                    }
                }
            }
        }
    }

    public void undo() {
        for (int y = 0; y < Screen.room.block.length; y++) {
            for (int x = 0; x < Screen.room.block[0].length; x++) {
                if (Screen.room.block[y][x].contains(mouseAction.prevMouse)) {
                        Screen.room.block[y][x].airId = mouseAction.heldId;
                        Screen.coinage -= mouseAction.price;

                }
            }
        }
    }
}
