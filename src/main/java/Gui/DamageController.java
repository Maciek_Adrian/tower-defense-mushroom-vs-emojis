package Gui;

/**
 * Created by Mlody on 2016-06-10.
 */
public class DamageController {

    private DamageHandler defaultDamageHandler;

    public void setDamageHandlers() {
        BossDamageHandler bossDamageHandler = new BossDamageHandler();
        HighDamageHander highDamageHander = new HighDamageHander();
        LowDamageHandler lowDamageHandler = new LowDamageHandler();

        lowDamageHandler.setNextDamageHandler(highDamageHander);
        highDamageHander.setNextDamageHandler(bossDamageHandler);
        this.defaultDamageHandler=lowDamageHandler;
    }

    public void giveDamage(Enemy enemy){
        if(defaultDamageHandler==null){
            setDamageHandlers();
        }
        defaultDamageHandler.handleDamage(enemy);
    }

}
