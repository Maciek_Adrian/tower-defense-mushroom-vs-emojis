package Gui;

import java.awt.*;

/**
 * Created by Mlody on 2016-05-21.
 */
public class Enemy extends Rectangle{
    public int xC, yC;
    public int enemySize= 52;
    public int enemyWalk=0;
    public int enemyHealth= 100, healthSpace = 3, healthHeight = 6;
    public int up=0, down =1, right=2, left=3;
    public int direction =right;
    public int enemyId= Value.ENEMY_AIR;
    public int damagePoints=5;
    public int walkSpeed =30;

    public boolean inGame=false;

    public Enemy(){

    }
    public void deleteEnemy(){
        inGame=false;
        direction=right;
        enemyWalk=0;
    }
    public void damageCastle(){
        Screen.damageController.giveDamage(this);
    }

    public void spawnEnemy(int enemyId){
        for(int y=0; y<Screen.room.block.length;y++){
            if(Screen.room.block[y][0].groundId==Value.GROUND_ROAD){
                setBounds(Screen.room.block[y][0].x, Screen.room.block[y][0].y,enemySize,enemySize);
                xC=0;
                yC=y;
            }
        }
        this.enemyId=enemyId;
        this.enemyHealth= enemySize;
        inGame=true;
    }

    public int walkFrame=0;
    public void physics(){
        if(walkFrame>=walkSpeed) {
            if(direction == right){
                x += 1;
            }else if(direction == down){
                y += 1;
            }else if(direction == up){
                y -= 1;
            }else if(direction == left){
                x -= 1;
            }
            enemyWalk +=1;
            if(enemyWalk == Screen.room.blockSize){
                if(direction == right){
                    xC += 1;
                }else if(direction == down){
                    yC += 1;
                }else if(direction == up){
                    yC -= 1;
                }
                else if(direction == left){
                    xC -= 1;
                }
                try{
                    if(Screen.room.block[yC][xC].airId== Value.AIR_CAVE){
                        deleteEnemy();
                        damageCastle();
                    }
                    if(Screen.room.block[yC+1][xC].groundId==Value.GROUND_ROAD && direction!= up){
                        direction=down;
                    }else if(Screen.room.block[yC-1][xC].groundId==Value.GROUND_ROAD && direction!= down){
                        direction=up;
                    }else if(Screen.room.block[yC][xC+1].groundId==Value.GROUND_ROAD && direction != left){
                        direction=right;
                    }else if(Screen.room.block[yC][xC-1].groundId==Value.GROUND_ROAD && direction != right){
                        direction=left;
                    }


                }catch (Exception e){}
                enemyWalk=0;
            }
            walkFrame=0;
        }else{
            walkFrame++;
        }
    }
    public void loseHealth(int amo){
        if(amo>=0){
        enemyHealth-=amo;
        }
        checkDeath();
    }
    public void checkDeath(){
        if(enemyHealth <= 0){
            deleteEnemy();
            Screen.killed++;
            Screen.coinage+= Value.DEATH_MONEY[enemyId];
        }
    }
    public boolean isDead(){
        if(inGame)return false;
        else return true;

    }

    public void draw(SimpleGraphics g){
        g.drawImage(Screen.tileset_enemy[enemyId],this);
        //Health bar
        g.setColor(new Color(180,50,50));
        g.fillRect(x,y-(healthSpace + healthHeight),width,healthHeight);

        g.setColor(new Color(50,180,50));
        g.fillRect(x,y-(healthSpace + healthHeight),enemyHealth,healthHeight);

        g.setColor(new Color(0,0,0));
        g.drawRect(x,y-(healthSpace + healthHeight),enemyHealth,healthHeight);
    }
}
