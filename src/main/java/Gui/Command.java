package Gui;

/**
 * Created by Mlody on 2016-06-10.
 */
public interface Command {
    public void execute(MouseAction mouseAction);
    public void undo();
}
