package Gui;

/**
 * Created by Mlody on 2016-06-10.
 */
public interface DamageHandler {
    public void setNextDamageHandler(DamageHandler damageHandler);
    public void handleDamage(Enemy enemy);

}
