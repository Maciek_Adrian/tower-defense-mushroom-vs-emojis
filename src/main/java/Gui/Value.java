package Gui;

/**
 * Created by Mlody on 2016-05-08.
 */
public class Value {

    public final static int GROUND_GRASS=0;
    public final static int GROUND_ROAD=1;

    public final static int AIR_EMPTY=-1;
    public final static int AIR_CAVE=0;
    public final static int AIR_TRASH=1;
    public final static int AIR_SHROOM1=2;
    public final static int AIR_SHROOM2=3;
    public final static int AIR_SHROOM3=4;
    public final static int AIR_SHROOM4=5;

    public final static int ENEMY_AIR=-1;
    public final static int ENEMY_BOSS=0;
    public final static int ENEMY_1=1;
    public final static int ENEMY_2=2;
    public final static int ENEMY_3=3;


    public final static int[] DEATH_MONEY={100,5,10,15};
    public final static int[] SHROOM_DAMAGE_POINTS={1,2,3,4,4,4,4};

}
