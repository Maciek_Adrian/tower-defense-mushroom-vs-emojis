package Gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Mlody on 2016-05-07.
 *
 * USE MOUSE LEFT CLICK FOR BUY ACTION
 * USE MOUSE RIGHT CLICK FOR SELL ACTION
 * USE MOUSE MIDDLE CLICK TO UNDO LAST ACTION
 *
 */
public class Frame extends JFrame {
    public static String title ="MUSHROOMS VS EMOJIS, LUST OF BLOOD";
    public static Dimension size= new Dimension(700,600);

    public Frame(){
        setTitle(title);
        setSize(size);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        init();
    }

    public void init(){
        setLayout(new GridLayout(1,1,0,0));
        Screen screen= new Screen(this);
        add(screen);

        setVisible(true);
    }

    public static void main(String[] args) {
        Frame frame=new Frame();
    }
}
