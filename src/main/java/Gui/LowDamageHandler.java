package Gui;

/**
 * Created by Mlody on 2016-06-10.
 */
public class LowDamageHandler implements DamageHandler{
    private DamageHandler nextDamageHandler;

    public void setNextDamageHandler(DamageHandler damageHandler) {
        nextDamageHandler=damageHandler;
    }

    public void handleDamage(Enemy enemy) {
        if(enemy.enemyId<2){
            Screen.health-=10;
        }else {
            nextDamageHandler.handleDamage(enemy);
        }
    }
}
