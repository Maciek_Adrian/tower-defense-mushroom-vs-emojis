package Gui;

import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Created by Mlody on 2016-05-20.
 */
public class KeyHandler implements MouseMotionListener, MouseListener{

    public void mouseClicked(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {
        try {
            Screen.store.click(e.getButton());
        } catch (BadLocationException e1) {
            e1.printStackTrace();
        }
    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }
    public void mouseDragged(MouseEvent e) {
        Screen.maus = new Point(e.getX() + (Frame.size.width-Screen.myWidth)/2,e.getY()+ ((Frame.size.height-Screen.myHeight)-(Frame.size.width-Screen.myWidth)/2));
    }
    public void mouseMoved(MouseEvent e) {
        Screen.maus = new Point((e.getX()) - ((Frame.size.width-Screen.myWidth)/2),(e.getY())- ((Frame.size.height-Screen.myHeight)-(Frame.size.width-Screen.myWidth)/2));
    }
}
