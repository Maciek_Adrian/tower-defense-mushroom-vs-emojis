package Gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Mlody on 2016-05-20.
 */
public class Save {

    private static Save saveInstance=null;

    private Save(){

    }

    public static Save getInstanceOfSave(){
        saveInstance=new Save();
        return saveInstance;
    }

    public void loadSave(File loadPath){
        try {
            Scanner scan = new Scanner(loadPath);

            while(scan.hasNext()){
                Screen.killedToWin=scan.nextInt();
                String[] enemies=scan.next().split("m");
                System.out.println(enemies[0]+""+enemies[1]+""+enemies[2]);

                Screen.addEnemyList(enemies);


                for(int y=0; y<Screen.room.block.length;y++){
                    for(int x=0; x<Screen.room.block[0].length;x++){
                        Screen.room.block[y][x].groundId = scan.nextInt();
                    }
                }
                for(int y=0; y<Screen.room.block.length;y++){
                    for(int x=0; x<Screen.room.block[0].length;x++){
                        Screen.room.block[y][x].airId = scan.nextInt();
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
