package Gui;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.text.AttributedCharacterIterator;

/**
 * Created by Mlody on 2016-06-09.
 */
public class SimpleGraphics {
    Graphics g;
    public SimpleGraphics(Graphics g){
        this.g=g;
    }

public void drawImage(Image image, Rectangle rectangle){
    g.drawImage(image,rectangle.x,rectangle.y,rectangle.width,rectangle.height,null);
}
    public void drawImage(Image image, Point point, Rectangle rectangle){
        g.drawImage(image,point.x,point.y,rectangle.width,rectangle.height,null);

    }
    public void drawImage(Image image,Block block){
        g.drawImage(image,block.x,block.y,block.width,block.height,null);

    }
   public void setFont(Font font){
       g.setFont(font);

   }
   public void setColor(Color color){
       g.setColor(color);
   }
    public void drawString(String message,Rectangle rectangle){
    g.drawString(message,rectangle.x,rectangle.y-10);
}
    public void drawString(String message,int x, int y){
        g.drawString(message,x,y);
    }
    public void fillRect(Rectangle rectangle){
        g.fillRect(rectangle.x,rectangle.y,rectangle.width,rectangle.height);
    }
    public void fillRect(int x, int y, int width, int height){
        g.fillRect(x,y,width,height);
    }
    public void drawLine(int x1,int x0, int y1, int size){
        g.drawLine(x1,x0,y1,size);

    }
    public void drawRect(int x, int y,int width, int height){
        g.drawRect(x,y,width,height);
    }


}
