package Gui;

import java.awt.*;

/**
 * Created by Mlody on 2016-05-07.
 */
public class Room {
    public int worldWidth=12;
    public int worldHeight=9;
    public int blockSize = 52;
    public Block[][] block;

    public Room(){
        define();
    }
    public void define(){
        block= new Block[worldHeight][worldWidth];

        for(int y=0;y<worldHeight;y++){
            for(int x=0;x<worldWidth;x++){
                block[y][x]=new Block((Screen.myWidth/2)-((worldWidth*blockSize)/2)+(x*blockSize),y*blockSize,blockSize,blockSize,Value.GROUND_GRASS,Value.AIR_EMPTY);
            }
        }
    }
    public void physics(){
        for (int y = 0; y < block.length; y++) {
            for (int x = 0; x < block[0].length; x++) {
                block[y][x].physics();
            }
        }
    }
    public void draw(SimpleGraphics g){
        for(int y=0;y<worldHeight;y++){
            for(int x=0;x<worldWidth;x++){
                block[y][x].draw(g);
            }
        }
        for(int y=0;y<worldHeight;y++){
            for(int x=0;x<worldWidth;x++){
                block[y][x].fight(g);
            }
        }
    }
}
