package Gui

import spock.lang.Specification

import java.awt.Point

/**
 * Created by Mlody on 2016-06-20.
 */
class SellCommandTest extends Specification {
    Screen screen;
    SellCommand command
    int ACTION_PRICE=100
    int HELD_ID=1

    def setup() {
        screen=new Screen();
        screen.define();
        command = new SellCommand();

    }
    def "test execute method"(){
        given:
        Screen.maus=new Point(-300,20)
        Screen.coinage=101
        Screen.room.block[0][0].groundId=Value.GROUND_GRASS
        Screen.room.block[0][0].airId=Value.AIR_SHROOM1


        when:
        command.execute(new MouseAction(HELD_ID,ACTION_PRICE))

        then:
        Screen.coinage==201

    }

    def "test undo method"(){
        given:
        Screen.maus=new Point(-300,20);
        Screen.coinage=101;
        Screen.room.block[0][0].groundId=Value.GROUND_GRASS
        command.mouseAction=new MouseAction(HELD_ID,ACTION_PRICE)

        when:
        command.undo()

        then:
        Screen.coinage==1
    }
}
