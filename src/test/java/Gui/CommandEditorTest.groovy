package Gui

import spock.lang.Specification

/**
 * Created by Mlody on 2016-06-16.
 */
class CommandEditorTest extends Specification {
    CommandEditor commandEditor;


    def setup() {
        commandEditor=new CommandEditor()
    }

    def "test undo when stack empty"(){
        when :
        commandEditor.undoLast()

        then:
        notThrown Exception
    }


}
