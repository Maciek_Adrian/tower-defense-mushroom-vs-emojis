package Gui

import spock.lang.Specification

/**
 * Created by Mlody on 2016-06-15.
 */
class DamageControllerTest extends Specification {
    DamageController controller;
    Enemy enemy;
    def setup() {
        controller=new DamageController()
        controller.setDamageHandlers();
        enemy = new Enemy();
    }

    def "test if after loseHealth method enemy health decreases"(){
        given:
        enemy.enemyId=x
        enemy.enemyHealth=20;
        def previousHealth=Screen.health;

        when:
        controller.giveDamage(enemy);

        then:
        previousHealth==Screen.health+y


        where:
        y|x
        10|1 //test low damage handler
        30|2 // test high damage handler
        50|3 // test boss damage handler
    }
}
