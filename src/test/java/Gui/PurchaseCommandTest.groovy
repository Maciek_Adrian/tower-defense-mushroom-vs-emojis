package Gui

import spock.lang.Specification

import java.awt.Point

/**
 * Created by Mlody on 2016-06-20.
 */



class PurchaseCommandTest extends Specification {
    Screen screen;
    PurchaseCommand command
    int ACTION_PRICE=100
    int HELD_ID=1

    def setup() {
        screen=new Screen();
        screen.define();
        command = new PurchaseCommand();

    }
    def "test execute method"(){
        given:
        Screen.maus=new Point(40,40)
        Screen.coinage=101
        Screen.room.block[0][0].groundId=Value.GROUND_GRASS

        when:
        command.execute(new MouseAction(HELD_ID,ACTION_PRICE))

        then:
        Screen.coinage==1;
    }

    def "test undo method"(){
        given:
        Screen.maus=new Point(40,40);
        Screen.coinage=101;
        Screen.room.block[0][0].groundId=Value.GROUND_GRASS
        command.mouseAction=new MouseAction(HELD_ID,ACTION_PRICE)

        when:
        command.undo()

        then:
        Screen.coinage==196
    }
}
