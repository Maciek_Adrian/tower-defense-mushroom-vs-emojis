package Gui

import spock.lang.Specification

/**
 * Created by Mlody on 2016-06-15.
 */
class EnemyTest extends Specification {

    Enemy enemy;


    def setup() {
        enemy = new Enemy();
        enemy.enemyId = 1;

    }

    def "test if after deleteEnemy Enemy still in game"() {

        when:
        enemy.deleteEnemy()

        then:
        enemy.inGame == false;

    }

    def "test if  when enemy has less than zero health number of killed enemies increase"() {

        given:
        enemy.enemyHealth = -7
        Screen.killed = 0


        when:
        enemy.checkDeath()

        then:
        Screen.killed == 1
    }

    def "test isDead method under different conditions"() {
        given:
        enemy.inGame = inGame

        when:
        enemy.isDead()

        then:
        enemy.isDead() == result


        where:
        inGame | result
        true   | false
        false  | true
    }



    def "test if after loseHealth method enemyhealth decreaases"(){
        given:
        def previousenemyHealth= enemy.enemyHealth

        when:
        enemy.loseHealth(x)

        then:
        enemy.enemyHealth<=previousenemyHealth


        where:
        y|x
        1|5
        1|10
        1|-5
    }





    def cleanup(){

    }
}
